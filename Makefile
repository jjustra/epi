install:
	mkdir -p /usr/local/share/pixmaps

	cp epi.py /usr/local/bin/epi
	cp epigui /usr/local/bin/
	cp epi.png /usr/local/share/pixmaps/

	chmod a+x /usr/local/bin/epi
	chmod a+x /usr/local/bin/epigui
