#!/bin/bash

# usage : epi [find_pattern%d [replace_string%d_%s ["dry" [epi_file_path]]]]
# "dry" flag will only print changes, not execute them
# epi also preserves file extension (and expects there be one, if not directory)

fnd="*[Ee]%02d*"
if [ -n "$1" ]
then
	fnd="$1"
fi

rep="%02d - %s"
if [ -n "$2" ]
then
	rep="$2"
fi

dry=""
if [ -n "$3" ]
then
	dry="$3"
fi

epi_path="./epi.txt"
if [ -n "$4" ]
then
	epi_path="$4"
fi


IFS=$'\n'
n=1

for s in $(cat "$epi_path")
do
	fn0=$(printf "$fnd" $n)
	fn1=$(printf "$rep" $n $s)

	if [ ! -e $fn0 ]
	then
		echo "#W : cannot find : $fn0 : skipping"
		n=$((n + 1))
		continue
	fi


	ext=""

	if [ -f $fn0 ]
	then
		ext=.$(echo $fn0 | rev | cut -d. -f1 | rev)
	fi


	if [ -n "$dry" ]
	then
		echo $fn0
		echo "   $fn1$ext"
	else
		mv $fn0 "$fn1$ext"
	fi

	n=$((n + 1))
done
