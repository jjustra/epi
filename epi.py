#!/usr/bin/python3


from glob import glob
import os
import sys


fnd = '*xE%02d*'
rep = 'test 123x%02d - %s'
dry = 0
epi_path = 'epi.txt'

if len(sys.argv) > 1:
	fnd = sys.argv[1]
if len(sys.argv) > 2:
	rep = sys.argv[2]
if len(sys.argv) > 3:
	dry = sys.argv[3]
if len(sys.argv) > 4:
	epi_path = sys.argv[4]


epii = 0

for epi in open(epi_path).readlines():
	epi = epi.strip()
	epii += 1
	_fnd = fnd % epii
	ifn = glob(_fnd)

	if not ifn:
		sys.stderr.write('#W :  cannot find : ' + _fnd + ' : skipping\n')
		continue

	ifn = ifn[0]
	ext = ''
	ofn = rep % (epii, epi)

	if '.' in ifn:
		ext = ifn.split('.')[-1]
		ofn += '.' + ext

	if dry:
		print(ifn)
		print('  ',ofn)
	else:
		os.rename(ifn,ofn)
